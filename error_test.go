package wrapper_test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/libs-artifex/wrapper/v2"
)

var (
	errNew    = errors.New("new error")
	errNotNew = errors.New("not new error")
)

func Test_errJSON_Is(t *testing.T) {
	t.Parallel()

	type args struct {
		err    error
		target error
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "happy path",
			args: args{
				err:    wrapper.Wrapf(errNew, "field = %d", 1),
				target: errNew,
			},
			want: true,
		},
		{
			name: "error no eq",
			args: args{
				err:    wrapper.Wrapf(errNew, "field = %d", 1),
				target: errNotNew,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			if errors.Is(tt.args.err, tt.args.target) != tt.want {
				t.Errorf("err = %s, target = %s, want %v", tt.args.err, tt.args.target, tt.want)
			}
		})
	}
}

type errHTTP struct {
	code    int
	message string
}

type noErrHTTP struct {
	number int
}

func (e *errHTTP) Error() string {
	return fmt.Sprintf("code: %d, message: %s", e.code, e.message)
}

func (e *noErrHTTP) Error() string {
	return fmt.Sprintf("number: %d", e.number)
}

func Test_errJSON_As(t *testing.T) {
	t.Parallel()

	t.Run("happy path", func(t *testing.T) {
		t.Parallel()

		err := wrapper.Wrapf(&errHTTP{
			code:    200,
			message: "OK",
		}, "field = %d", 1)

		var target *errHTTP
		if !errors.As(err, &target) {
			t.Errorf("err = %#v, target = %#v", err, target)
		}
	})

	t.Run("no error", func(t *testing.T) {
		t.Parallel()

		err := wrapper.Wrapf(&errHTTP{
			code:    200,
			message: "OK",
		}, "field = %d", 1)

		var target *noErrHTTP
		if errors.As(err, &target) {
			t.Errorf("err = %#v, target = %#v", err, target)
		}
	})

	t.Run("fmt.Errorf", func(t *testing.T) {
		t.Parallel()

		err := wrapper.Wrapf(fmt.Errorf("some err %w", &errHTTP{
			code:    200,
			message: "OK",
		}), "field = %d", 1)

		var target *errHTTP
		if !errors.As(err, &target) {
			t.Errorf("err = %#v, target = %#v", err, target)
		}
	})
}
